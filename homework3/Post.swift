//
//  Post.swift
//  homework3
//
//  Created by Sereyvong Thorn on 11/23/18.
//  Copyright © 2018 Sereyvong Thorn. All rights reserved.
//

import Foundation

class Post {
    var user:String?
    var imageProfile:String?
    var postImage:String?
    var like:String?
    var description:String?
    
    static func listPosts() -> [Post] {
        var listPosts:[Post] = [Post]()
        var post = Post()
        post.user = "thorn_sereyvong"
        post.imageProfile = "sereyvong.png"
        post.postImage = "sereyvong_post1.jpg"
        post.like = "1.4 K"
        post.description = "hello from Kompot"
        listPosts.append(post)
        
        post = Post()
        post.user = "kheav_chansotheara"
        post.imageProfile = "theara.png"
        post.postImage = "theara_post1.jpg"
        post.like = "1 like"
        post.description = "hello from Siem Reap"
        listPosts.append(post)
        
        return listPosts
    }
}
