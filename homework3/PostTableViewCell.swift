//
//  PostTableViewCell.swift
//  homework3
//
//  Created by Sereyvong Thorn on 11/23/18.
//  Copyright © 2018 Sereyvong Thorn. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var overflowImage: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    func configureCell(post:Post) {
        usernameLabel.text = post.user
        likeLabel.text = post.like
        descriptionLabel.text = post.description
        profileImage.image = #imageLiteral(resourceName: "user")
        postImage.image = #imageLiteral(resourceName: "sereyvong_post1")
        
        overflowImage.setImage(#imageLiteral(resourceName: "more"), for: .normal)
        heartButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        commentButton.setImage(#imageLiteral(resourceName: "comment-white-oval-bubble (1)"), for: .normal)
        shareButton.setImage(#imageLiteral(resourceName: "telegram"), for: .normal)
        moreButton.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
    }
    

}
